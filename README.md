# NO CODE

This is proven to be the best code that has ever been written by a human being

## Feature

- Always compile

- No interpreter, JVM, compiler, etc... needed

- Never a crash

- Optimal for production

- Fast execution time at 0.00s

- Cross Platform with all kind of Operating Systems and devices

- No dependencies needed


## Compile

Do nothing, stare at your computer and relax or have a coffe

## Execution

Do nothing, stare at your computer and relax or have a coffe

## Testing

Do nothing, stare at your computer and relax or have a coffe